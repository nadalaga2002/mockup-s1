const customer = require('../src/controller/s1.controller');

const app = require('../src/index');
const supertest = require('supertest');
const request = supertest(app);

it('Address: if Street not null', async () => {
  let address = {
    address: '123 test',
    unit: '1',
    poBox: 'poBox test',
    privateBag: 'dd'
  };
  expect(customer.processAddress(address)).toBe('Unit 1,123 test');
});

it('Address: if Street is null', () => {
  let address = {
    address: '',
    unit: '1',
    poBox: 'poBox test',
    privateBag: 'dd'
  };
  expect(customer.processAddress(address)).toBe('poBox test');
});
