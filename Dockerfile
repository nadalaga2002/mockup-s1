FROM ubuntu

ARG ENV=development
ENV NODE_ENV ${ENV}

RUN apt-get update
RUN apt-get -y install curl gnupg
RUN curl -sL https://deb.nodesource.com/setup_11.x  | bash -

RUN npm install
RUN node --version
RUN npm --version

COPY package.json package-lock.json  ./
RUN apt-get -y install nodejs
RUN npm install

COPY . .
RUN npm run compile

EXPOSE 7020
CMD npm start