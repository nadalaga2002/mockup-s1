export const config = {
    mappingAPI: process.env.MAPPING_API || 'http://localhost:7010/m2/s2',
    env: process.env.NODE_ENV || 'development',
    port: process.env.PORT || 8060
};
