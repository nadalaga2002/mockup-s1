import express from 'express';
import {  pushDataToS3 } from './controller/s1.controller';
const router = express.Router();


router.put(
  '/customer',
  pushDataToS3
);

export default router;
