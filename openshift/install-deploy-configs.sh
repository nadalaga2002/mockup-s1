#!/bin/bash

echo "Alrighty now I'm going to install all those openshift configs"
echo "...Before we continue please make sure your openshift cluster is up and running."
echo "If you're not sure how to do this, just type 'oc cluster up' and wait for it to start properly before continuing."
echo "You're about to asked a bunch of questions about your deployemnt, just accept the defaults if you're not sure."
echo "Are you read to continue? y/n"

read continue;

if [ "$continue" == "y" ]; then

    echo 'Project Name: (default: mockup)'
    read PROJECT_NAME

    if [ -z "$PROJECT_NAME" ]; then
        PROJECT_NAME='mockup'
    fi

    echo 'Service 1 Name: (default: mockup-s1)'
    read SERVICE_NAME1

    if [ -z "$SERVICE_NAME1" ]; then
        SERVICE_NAME1='mockup-s1'
    fi


    echo 'Service Git Url: (default: https://gitlab.com/nadalaga2002/mockup-s1.git)'
    read SERVICE_GIT_URL1

    if [ -z "$SERVICE_GIT_URL1" ]; then
        SERVICE_GIT_URL1='https://gitlab.com/nadalaga2002/mockup-s1.git' #git@gitlab.com:nadalaga2002/mockup-s1.git
    fi

    echo 'Service 2 Name: (default: mockup-s2)'
    read SERVICE_NAME2

    if [ -z "$SERVICE_NAME2" ]; then
        SERVICE_NAME2='mockup-s2'
    fi

    echo 'Service Git Url: (default: https://gitlab.com/nadalaga2002/mockup-s2.git)'
    read SERVICE_GIT_URL2

    if [ -z "$SERVICE_GIT_URL2" ]; then
        SERVICE_GIT_URL2='https://gitlab.com/nadalaga2002/mockup-s2.git' #git@gitlab.com:nadalaga2002/mockup-s1.git
    fi

    echo 'Environment: (default: dev)'
    read ENVIRONMENT

    if [ -z "$ENVIRONMENT" ]; then
        ENVIRONMENT='dev'
    fi

    echo 'User Name: (default: nadalaga2002)'
    read USER_NAME

    if [ -z "$USER_NAME" ]; then
        USER_NAME='nadalaga2002'
    fi

    echo 'Password: (default: password)'
    read -s PASSWORD

    if [ -z "$PASSWORD" ]; then
        PASSWORD='Admin@1234'
    fi

    echo "Looking good, I'm about to configure openshift for your service $SERVICE_NAME as user '$USER_NAME' under project '$PROJECT_NAME' for environment '$ENVIRONMENT'"
    echo "Does this look good to you y/n?"
    read continue;

    if [ "$continue" == "y" ]; then
        # Login and select project
        oc login -u $USER_NAME -p $PASSWORD https://console.pro-ap-southeast-2.openshift.com
        oc new-project $PROJECT_NAME

        ########### Start Service 1
        # Install ConfigMaps
        oc create configmap $SERVICE_NAME1-$ENVIRONMENT-config -n $PROJECT_NAME --from-file config/$ENVIRONMENT/runtime-config/application.properties

        # Install Services
        oc process -f config/$ENVIRONMENT/svc-s1.yaml -n $PROJECT_NAME -p SERVICE_NAME=$SERVICE_NAME1 | oc create -f - -n $PROJECT_NAME

        # Install Routes
        oc process -f config/$ENVIRONMENT/route-s1.yaml -n $PROJECT_NAME -p SERVICE_NAME=$SERVICE_NAME1 | oc create -f - -n $PROJECT_NAME

        # Install Deploy configs
        oc process -f config/$ENVIRONMENT/dc-s1.yaml -n $PROJECT_NAME -p PROJECT_NAME=$PROJECT_NAME -p SERVICE_NAME=$SERVICE_NAME1 -p ENVIRONMENT=$ENVIRONMENT | oc create -f - -n $PROJECT_NAME

        ############ Start Mockup Service 2 #################
        # Install ConfigMaps
       # oc create configmap $SERVICE_NAME2-$ENVIRONMENT-config -n $PROJECT_NAME --from-file config/$ENVIRONMENT/runtime-config/application.properties

        # Install Services
        #oc process -f config/$ENVIRONMENT/svc-s2.yaml -n $PROJECT_NAME -p SERVICE_NAME=$SERVICE_NAME2 | oc create -f - -n $PROJECT_NAME

        # Install Routes
        #oc process -f config/$ENVIRONMENT/route-s2.yaml -n $PROJECT_NAME -p SERVICE_NAME=$SERVICE_NAME2 | oc create -f - -n $PROJECT_NAME

        # Install Deploy configs
        #oc process -f config/$ENVIRONMENT/dc-s2.yaml -n $PROJECT_NAME -p PROJECT_NAME=$PROJECT_NAME -p SERVICE_NAME=$SERVICE_NAME2 -p ENVIRONMENT=$ENVIRONMENT | oc create -f - -n $PROJECT_NAME

    fi
fi