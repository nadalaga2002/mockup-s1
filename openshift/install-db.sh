#!/bin/bash

echo "...Before we continue please make sure your openshift cluster is up and running."
echo "If you're not sure how to do this, just type 'oc cluster up' and wait for it to start properly before continuing."
echo "Are you read to continue? y/n"

read continue;

if [ "$continue" == "y" ]; then
    
    echo 'OpenShift URL: (default: https://console.pro-ap-southeast-2.openshift.com)'
    read OPENSHIFT_URL

    if [ -z "$OPENSHIFT_URL" ]; then
        OPENSHIFT_URL='https://console.pro-ap-southeast-2.openshift.com'
    fi

    echo 'Project Name: (default: postgres)'
    oc project
    read PROJECT_NAME

    if [ -z "$PROJECT_NAME" ]; then
        PROJECT_NAME='postgres'
    fi

    echo 'Username: (default: nadalaga2002)'
    read -s PASSWORD
    if [ -z "$USER_NAME" ]; then
        USER_NAME='nadalaga2002'
    fi

    echo 'Password: (default: password)'
    read -s PASSWORD

    if [ -z "$PASSWORD" ]; then
        PASSWORD='Admin@1234'
    fi

    echo 'POSTGRESQL_DATABASE: (default: mockup)'
    read POSTGRESQL_DATABASE

    if [ -z "$POSTGRESQL_DATABASE" ]; then
        POSTGRESQL_DATABASE='mockup'
    fi

    echo 'POSTGRESQL_PASSWORD: (default: mockup)'
    read POSTGRESQL_PASSWORD

    if [ -z "$POSTGRESQL_PASSWORD" ]; then
        POSTGRESQL_PASSWORD='mockup'
    fi

    echo 'POSTGRESQL_USER: (default: mockup)'
    read -s POSTGRESQL_USER

    if [ -z "$POSTGRESQL_USER" ]; then
        POSTGRESQL_USER='mockup'
    fi

    echo "Looking good, I'm about to configure openshift for your postgres database service with $POSTGRESQL_PASSWORD as user '$POSTGRESQL_USER' under database '$POSTGRESQL_DATABASE'"
    echo "Does this look good to you y/n?"
    read continue;

    if [ "$continue" == "y" ]; then
        # Login and select project
        oc login -u $USER_NAME -p $PASSWORD  $OPENSHIFT_URL #https://192.168.64.3:8443/console
        oc new-project $PROJECT_NAME
        oc new-app docker-registry.default.svc:5000/openshift/postgresql -e POSTGRESQL_USER=$POSTGRESQL_USER -e POSTGRESQL_PASSWORD=$POSTGRESQL_PASSWORD -e POSTGRESQL_DATABASE=$POSTGRESQL_DATABASE
    fi
fi
