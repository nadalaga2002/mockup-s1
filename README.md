# Mockup

## Coding Principle and Convention

- Variable name must be camelCase
- Use lower-kebab-case filenames
  + Npm forbids uppercase in new package names
  + This format avoids filesystem case sensitivity issues accross different platforms 